from flask.ext.wtf import Form
from wtforms import StringField, HiddenField, SubmitField
from warpd import app


class FormStream(Form):

    mode = HiddenField()
    name = StringField('Name')
    url = StringField('URL')
    submit = SubmitField('Save')


class FormStreamDelete(Form):

    
    submit = SubmitField('Yes')
