import mpd
import sqlite3

from time import time
from os.path import exists
from functools import wraps
from json import dumps as json
from collections import OrderedDict
from time import sleep
from flask import Flask, g, request, url_for, render_template, flash, redirect


app = Flask(__name__)
app.config['reconnect_delay'] = 10 # reconnect delay in seconds

client = False
ResponseMap = {}

import forms


class Response:

    def __init__(self, url, payload_callback, **kwargs):
     
        @app.route(url, **kwargs)
        @wraps(payload_callback)
        def callback(*args, **kwargs):
            return self.render(payload_callback(*args, **kwargs))

        ResponseMap[url] = callback


    def render(self, value):
        if type(value).__name__ == 'Response':
            return value
        elif request.referrer:
            return redirect(request.referrer)
        return json(value)


class Page(Response):

    def render(self, value):
        
        if type(value).__name__ == 'Response':
            return value

        g.page['content'].append(value)

        return render_template('index.jinja', page=g.page)




class warpd(mpd.MPDClient):

    server = None
    time_connected = None

    def __init__(self, host='localhost', port=6600, password=False):

        super(warpd, self).__init__(self)
        self.connect(host, port)
        self.server = {
            'host': host,
            'port': port
        }

        if(password):
            self.password(password)

        self.time_connected = time()

    
    def _write_command(self, *a, **kw):

        if time() - self.time_connected >= app.config['reconnect_delay']:
            self.reconnect()
            print "reconnected!"
        return super(warpd, self)._write_command(*a, **kw)


    def stream(self):

        song = self.currentsong()

        if song:
            url = song['file']

            try:
                stream = Stream(url=url)
            except Exception, e: #TODO exception type
                raise
                return False

            return stream

        return False


    def setvol(self, vol, duration = 300):
        
        granularity = 10
        delay = float(granularity) / 1000
        cvol = int(self.status()['volume'])

        if not cvol == vol:

            if duration == 0:
                super(warpd, self).setvol(vol)

            else:

                steps = duration/granularity
                step = (vol - cvol)/float(steps)


                for i in range(1,  steps):

                    v = int(cvol + round(i*step))

                    super(warpd, self).setvol(v)
                    sleep(delay)

                super(warpd, self).setvol(vol)

    
    def reconnect(self):

        try:
            self.disconnect()
            self.connect(self.server['host'], self.server['port'])
            self.time_connected = time()
        except mpd.ConnectionError as e:
            pass


class Stream:

    #TODO: MOAR EXCAPSHIUNS

    id = None
    name = None
    url = None

    def __init__(self, id = None, url = None):
        
        if id or url:
            if id:
                r = db_query('SELECT * FROM `streams` WHERE `id` = ?', [id])
            elif url:
                r = db_query('SELECT * FROM `streams` WHERE `url` = ?', [url])

            if len(r) > 0:

                row = r[0]

                self.id = row['id']
                self.name = row['name']
                self.url = row['url']


    def save(self):

        if self.name and self.url:
            if self.id:
                db_query('UPDATE `streams` SET `name` = ?, `url` = ? WHERE `id` = ?', (self.name, self.url, self.id))
                db_commit()

            else:
                res = g.dbcon.execute('INSERT INTO `streams` (`name`, `url`) VALUES (?, ?)', (self.name, self.url))
                db_commit()
                self.id = int(res.lastrowid)

            return True

        return False


    def delete(self):

        if self.id:
            res = g.dbcon.execute('DELETE FROM `streams` WHERE id = ?', (self.id,))
            db_commit()
            return True

        return False


    def play(self):
        if self.id:

            global client
            cvol = int(client.status()['volume'])
            client.setvol(0, 300)
            sleep(0.3)
            client.clear()
            client.add(self.url)
            client.play()
            client.setvol(cvol)

            return True

        return False



# custom exceptions below this

class DatabaseError(Exception):
    pass


import elements as e
import volbar

Modules = {'volbar': volbar} # Module collection for callbacks (currently only before_request())


@app.before_first_request
def boot():

    global client
    client = warpd()



@app.before_request
def request_init():

    try:
        g.dbcon = db_connect()
    except:
        raise

    global client
    #client.reconnect()
    
    g.page = OrderedDict()
    g.data = {}

    menu_main = e.Menu(items=[
        e.Link(url='/', label='Home'),
        e.Link(url='/streams/', label='Streams')],
        
        attributes={'class': 'main'})

    status = client.status()
    song = client.currentsong()

    g.data['status'] = status
    g.data['song'] = song

    main_classes = [
        'state-%s' % (status['state']),
        'vol-%d' % (int(status['volume'])),
        'repeat-%s' % ('true' if int(status['repeat']) == 1 else 'false'),
        'random-%s' % ('true' if int(status['random']) == 1 else 'false'),
        'single-%s' % ('true' if int(status['single']) == 1 else 'false'),
        prettify('genre-%s' % (song['genre'] if song.has_key('genre') else 'none')),
        'active'
    ]

    main_classes = ' '.join(main_classes)
   
    g.page['classes'] = main_classes
    g.page['header'] = e.Container(items=[menu_main])
    g.page['content'] = e.Container()
    g.page['sidebar'] = e.Container()
    g.page['footer'] = e.Container(items=[
        e.Player(status=client.status(),
        song=client.currentsong(),
        stream=client.stream())])

    g.page['frame'] = {
        'top': e.Container(),
        'bottom': e.Container(),
        'left': e.Container(),
        'right': e.Container()
    }

    for name, mod in Modules.iteritems():
        if hasattr(mod, 'before_request') and callable(mod.before_request):
            mod.before_request()




def prettify(string):

    return string.lower().replace(' ', '-').replace('_', '-').replace('&', 'and')


def db_connect(force = False):
    if exists(app.config['database']) or force:
        return sqlite3.connect(app.config['database'])
    else:
        raise DatabaseError("Database doesn't exist.")


def db_close():
    if hasattr(g, 'dbcon') and g.dbcon:
        g.dbcon.close()


def db_init():
    with closing(db_connect(True)) as db:
        with app.open_resource('schema.sql') as f:
            db.cursor().executescript(f.read())
        db.commit()


# Shit my brain can't even parse this, shamelessly copied from http://flask.pocoo.org/docs/patterns/sqlite3/
def db_query(query, args=(), one=False):
    cur = g.dbcon.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
        for idx, value in enumerate(row)) for row in cur.fetchall()]

    return (rv[0] if rv else None) if one else rv


def db_commit():
    r = g.dbcon.commit()
    

def getargs():

    args = {}

    for k, v in request.args.iteritems():
        args[k] = v

    for k, v in request.form.iteritems():
        args[k] = v

    return args



def response_status():

    global client

    return client.status()


def response_play():

    global client

    cvol = int(client.status()['volume'])

    client.setvol(0, 0)
    client.play()
    client.setvol(cvol)

    return True #TODO: error reporting


def response_pause():

    global client

    cvol = int(client.status()['volume'])

    client.setvol(0, 300)
    sleep(0.3)
    client.pause()
    client.setvol(cvol, 0)


def response_stop():

    global client

    cvol = int(client.status()['volume'])

    client.setvol(0, 300)
    sleep(0.3)
    client.stop()
    client.setvol(cvol, 0)


def response_setvol():

    global client

    args = getargs() # This is ugly

    if args.has_key('vol') and args['vol'].isdigit() and not int(args['vol']) > 100 and not int(args['vol']) < 0:
        if args.has_key('duration') and args['duration'].isdigit():
            client.setvol(int(args['vol']), int(args['duration']))
        else:
            client.setvol(int(args['vol']))


    return True


def response_stream(id): #TODO: FIXME
    
    args = getargs()

    if args.has_key('name') and args.has_key('url'):
        if args.has_key('id'):
            s = Stream(int(args['id']))
            s.name = args['name']
            s.url = args['url']

        else:
            s = Stream()
            s.name = args['name']
            s.url = args['url']

        return s.save()

    return False


def response_stream_play(id):

    s = Stream(id)
    return s.play()



def page_root():

    return e.Image(url='/static/images/logo.png')


def page_streams():

    r = db_query('SELECT * FROM streams')

    header = ['name (click to play)', 'actions', 'edit']

    rows = []

    for row in r:
        rows.append([
            e.Link(label=row['name'], url='/api/streams/%d/play' % (row['id'])),
            e.Text(''),
            e.Link(label='edit', url='/streams/%d' % (row['id'])),
        ])


    table = e.Table(header=header, rows=rows)

    return e.Container(items=[table, e.Link(label='Add new stream', url='/streams/add')])


def page_stream(id):

    s = None
    f = forms.FormStream()

    if request.method == 'POST':
        if request.form['mode'] == 'add':
            s = Stream()
            s.name = request.form['name']
            s.url = request.form['url']

            if s.save():
                flash('Added new stream: %s' % (s.name))
                return redirect( '/streams/%d' % (s.id))
            else:
                flash('Failed to add stream: %s' % (s.name))

        elif request.form['mode'] == 'edit':
            s = Stream(int(id))
            s.name = request.form['name']
            s.url = request.form['url']

            if s.save():
                flash('Updated stream: %s' % (s.name))
                redirect('/streams/')
            else:
                flash('Failed to update stream: %s' % (s.name))


    if id == 'add':
        f.mode.data = 'add'
    else:
        f.mode.data = 'edit'
        s = Stream(id)

        f.name.data = s.name
        f.url.data = s.url

    build = e.Container(items=[e.Form(form=f)])

    if s:

        build.append(e.Link(label='Delete', url='/streams/%d/delete' % (int(s.id))))

    return build


def page_stream_delete(id):
    
    s = Stream(int(id))

    if request.method == 'POST':
        if request.form['submit'] == 'Yes':
            name = s.name
            if s.delete():
                flash('Stream successfully deleted: %s' % (name))
                return redirect('/streams/')

            else:
                flash('Could not delete stream: %s' % (name))

    f = forms.FormStreamDelete()

    return e.Container(items = [
        e.Text("Really delete stream '%s'?" % (s.name)),
        e.Form(form=f)
    ])
