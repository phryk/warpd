#import logging
from flask.ext.sass import Sass 

from settings import *
from warpd import *


if __name__ == '__main__':

    #logging.basicConfig()
    #app.logger.addHandler(logging.FileHandler('warpd.log'))


    Response('/api/play', response_play)
    Response('/api/pause', response_pause)
    Response('/api/stop', response_stop)
    Response('/api/setvol', response_setvol)
    Response('/api/streams/<id>', response_stream)
    Response('/api/streams/<id>/play', response_stream_play)

    root = Page('/', page_root)
    streams = Page('/streams/', page_streams)
    stream = Page('/streams/<id>', page_stream, methods = ['GET', 'POST'])
    stream_delete = Page('/streams/<id>/delete', page_stream_delete, methods = ['GET', 'POST'])

    sass = Sass(app)
    app.run(host='0.0.0.0')

    #app.logger.debug('started foo!')
