from collections import OrderedDict
from copy import copy

from flask import render_template
import warpd



class ElementException(Exception):
    pass


class TypeList(list):

    pass


class Element(object):

    __required__ = {}
    __optional__ = {'attributes': {}}



    def __getitem__(self, key):

        try:
            return self.data[key]
        except: raise


    def __setitem__(self, key, value):

        try:
            if self.typecheck(key, value):
                self.data[key] = value
                return True
            else:
                return False

        except: raise

    def keys(self):
        return self.data.keys()


    def __init__(self, *args, **kwargs):

        self.data = OrderedDict()

        own_type = type(self).__name__

        for key in self.__required__.keys():

            if key in kwargs.keys():

                value = kwargs[key]

                if self.typecheck(key, value):
                    self[key] = value
                    
            else:
                msg = "Missing required kwarg '%s' in element type %s." \
                    % (key, own_type)

                raise ElementException(msg)


        for key, default in self.__optional__.iteritems():

            self.__optional__[key] = copy(default)

            if key in kwargs.keys():

                value = kwargs[key]

                if self.typecheck(key, value):
                    self[key] = value

            else:
                self[key] = default

        self.init(*args, **kwargs)


    def typecheck(self, key, value):

        typedef = False

        if type(value).__name__ == 'instance':
            real_type = value.__class__
        else:
            real_type = type(value)


        if self.__required__.has_key(key):

            typedef = self.__required__[key]

            if (
                typedef == 'any' or
                (type(typedef) is TypeList and real_type in typedef) or
                (type(typedef) is not TypeList and real_type is typedef)
            ):
                return True

        elif self.__optional__.has_key(key):

            typedef = self.__optional__[key]

            if (
                typedef == 'any' or
                (type(typedef) is TypeList and 
                    (real_type in typedef or
                     real_type in [type(x) for x in typedef]
                    )
                ) or
                (type(typedef) is not TypeList and
                    (real_type is typedef or
                     real_type is type(typedef))
                )
            ):
                return True

        if not warpd.app.config['DEBUG']:
            return False

        msg = ''

        if typedef: 

            if self.__required__.has_key(key):

                if type(typedef) is TypeList and real_type not in typedef:

                    msg = "Required %s['%s'] has wrong type: %s. Should be one of %s" % (
                        self.__class__.__name__,
                        key,
                        real_type.__name__,
                        ', '.join([x.__name__ for x in typedef])
                    )

            if self.__optional__.has_key(key):

                if real_type is not typedef and real_type is not type(typedef):
            
                    msg = "Optional %s['%s'] has wrong type should be one of %s" % (
                        self.__class__.__name__,
                        key,
                        ', '.join([type(default).__name__, type])
                    )

            else:
                msg = 'This should not happen. Typecheck error in %s' % (self.__class__.__name__)
        else:

            msg = "Key '%s' not allowed in %s" % (key, self.__class__.__name__)

        raise ElementException(msg)

        return False

    
    def empty(self):

        return len(self.data) == 0


    def init(self, *args, **kwargs):
        pass


    def render(self):

        if self.empty():

            return ''

        name = type(self).__name__

        if name == 'classobj':
            name = self.__name__
        elif name == 'instance':
            name = self.__class__.__name__

        template = "%s.jinja" % (name)

        return render_template(template, data=self)


    def attributes(self):
        return render_template('attributes.jinja', attributes=self['attributes'])




class Text:

    def __init__(self, text):
        self.text = text

    def render(self):

        return self.text


class Link(Element):

    __required__ = {'url': 'any', 'label': 'any'} #both could be str or unicode; does python have roles/interfaces/foo?


class Player(Element):

    __required__ = {'status': dict, 'song': dict}
    __optional__ = {
        'attributes': {'class': 'player'},
        'stream': TypeList([warpd.Stream, bool])
    }

    def __init__(self, *args, **kwargs):
        if not kwargs.has_key('stream'):
            kwargs['stream'] = {} #FIXME: find a better way to set a default
        super(Player, self).__init__(self, *args, **kwargs)


class Table(Element):

    __optional__ = {'attributes': {}, 'header': [], 'rows': []}


class Form(Element):

    __required__ = {'form': 'any'}
    __optional__ = {'attributes': {}, 'method': 'POST', 'action': ''}


class Header(Element):

    __optional__ = {'attributes': {}, 'fortune': 'Server depressed, needs Prozac'}


class Menu(Element):

    __optional__ = {'attributes': {}, 'items': []}


class Image(Element):

    __required__ = {'url': str}
    __optional__ = {'attributes': {}, 'alt': '', 'title': ''}


class Container(Element):

    __optional__ = {'attributes': {}, 'items': []}

    def append(self, item):
        self['items'].append(item)

    def empty(self):

        return len(self['items']) == 0
