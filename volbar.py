from flask import g
from warpd import client
import elements as e

def before_request():

    volume = int(g.data['status']['volume'])

    links = []

    r = range(1, 101)
    r.reverse()

    for i in r:
        
        classes = 'vol vol-%d' % (i)

        if i <= volume:
            classes += ' active'

        links.append(e.Link(label=str(i), url='/api/setvol?vol=%d&duration=300' % (i), attributes={'class': classes}))

    g.page['frame']['right'].append(e.Container(items=links, attributes={'class': 'volbar'}))
