# Setup

## Installing Requirements

    pip install Flask
    pip install Flask-Sass
    pip install Flask-WTF

Additionally a Sass implementation is needed. It doesn't matter which one  
as long as it supplies a sass-compatible binary.


## Create empty database

    cat schema.sql | sqlite3 db.db


## Set sass binary path, if needed

The default path for the sass binary is /usr/local/bin/sass.
If your binary is found somewhere else, set the corresponding
configuration variable in `settings.py` accordingly.

    app.config['SASS_BIN_PATH'] = '/path/to/sass'


## Run application in debug mode

    python start.py


# Note

This software hasn't been deployed anywhere yet and has no security features  
whatsoever. DO NOT deploy this on public servers without extra protection.
