DROP TABLE IF EXISTS `streams`;
CREATE TABLE `streams` (
    id integer primary key autoincrement,
    name text not null unique,
    url text not null unique
);
